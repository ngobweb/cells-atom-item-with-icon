# cells-atom-item-with-icon

Your component description.

Example:
```html
<cells-atom-item-with-icon></cells-atom-item-with-icon>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-atom-item-with-icon-scope      | scope description | default value  |
| --cells-atom-item-with-icon  | empty mixin     | {}             |
