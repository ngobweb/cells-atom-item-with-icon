(function() {

  'use strict';

  Polymer({

    is: 'cells-atom-item-with-icon',

    behaviors: [
      Polymer.i18nBehavior
    ],
    properties: {
      /**
      *Icon code that's gonna show
      *@type {String}
      *@public
      */
      icon: {
        type: String,
        value: null
      },
      /**
      *Icon size that's gonna show
      *@type {Number/String}
      *@public
      */
      iconSize: {
        type: Number,
        value: 16
      },
      /* Amount data
      * @type {Object}
      * @public
      */
      amount: {
        type: Number
      },
      /* Currency code of amount
      * @type {String}
      * @public
      */
      currencyCode: {
        type: String
      },
      /* Local currency
      * @type {String}
      * @public
      */
      localCurrency: {
        type: String
      },
      /* Date data
      *@type {String}
      * @public
      */
      date: {
        type: String
      },
      /* Date data
      * @type {String}
      * @public
      */
      formatDate: {
        type: String,
        value: 'LL'
      },
      /* Text data
      * @type {String}
      * @public
      */
      text: {
        type: String
      },
      /* Number data
      * @type {Number}
      * @public
      */
      number: {
        type: Number
      },
      /* Description data
      * @type {String}
      * @public
      */
      description: {
        type: String
      }
    },
    /**
    * checked if has string
    */
    _checkedValue: function(str) {
      return Boolean(str);
    },
    /**
    * Chekced type
    */
    _checkedType: function(type, defaultType) {
      return (type === defaultType);
    }
  });
}());
