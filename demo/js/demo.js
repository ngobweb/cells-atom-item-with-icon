var templateBind = document.getElementById('tbind');
var amount = 4444;
var localCurrency = 'MXN';
var currency ='MXN';
var date = '2016-08-18';
var formatDate = 'LL';

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  templateBind.set('amount', amount);
  templateBind.set('localCurrency', localCurrency);
  templateBind.set('currency', currency);
  templateBind.set('date', date);
  templateBind.set('description', 'Date Balance');
  templateBind.set('descriptionAmount', 'Amount Balance');
  templateBind.set('formatDate', formatDate);
});
